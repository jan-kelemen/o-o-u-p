# OOuP - Oblikovni obrasci u programiranju (Design Patterns in Software Design)
This folder contains solutions to lab tasks of Design Patterns in Software Design course.

## Content
* LAB01 - Polymorphism in C and C++, SOLID principles
* LAB02 - SOLID principles, strategy, observer, decorator
* LAB03 - Factory, Text editor -- MOSTLY FINISHED
* LAB04 - Paint -- NOT FINISHED
