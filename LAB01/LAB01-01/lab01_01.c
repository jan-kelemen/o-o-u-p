#include <stdio.h>
#include <stdlib.h>

typedef char const* (*PTRFUN)();

typedef struct
{
    char const* name;
    PTRFUN* vtable;
} Animal;

void animal_print_greeting(Animal const*);
void animal_print_menu(Animal const*);

Animal* create_dog(char const*);
void construct_dog(char const*, Animal*);
char const* dog_greet();
char const* dog_menu();
PTRFUN dog_vtable[2] = {dog_greet, dog_menu};

Animal* create_cat(char const*);
void construct_cat(char const*, Animal*);
char const* cat_greet();
char const* cat_menu();
PTRFUN cat_vtable[2] = {cat_greet, cat_menu};

void test_animals();

Animal stack_alloc_cat(char const* name);

Animal* create_n_dogs(int);

int main(int argc, char* argv[])
{
    test_animals();
    system("PAUSE");
    return EXIT_SUCCESS;
}

void animal_print_greeting(Animal const* animal) { printf("%s pozdravlja: %s\n", animal->name, animal->vtable[0]()); }

void animal_print_menu(Animal const* animal) { printf("%s voli %s\n", animal->name, animal->vtable[1]()); }

Animal* create_dog(char const* name)
{
    Animal* animal = (Animal*) malloc(sizeof(Animal));
    construct_dog(name, animal);
    return animal;
}

void construct_dog(char const* name, Animal* animal)
{
    animal->name = name;
    animal->vtable = dog_vtable;
}

char const* dog_greet() { return "vau!"; }

char const* dog_menu() { return "kuhanu govedinu"; }

Animal* create_cat(char const* name)
{
    Animal* animal = (Animal*) malloc(sizeof(Animal));
    construct_cat(name, animal);
    return animal;
}

void construct_cat(char const* name, Animal* animal)
{
    animal->name = name;
    animal->vtable = cat_vtable;
}
char const* cat_greet() { return "mijau!"; }

char const* cat_menu() { return "konzerviranu tunjevinu"; }

void test_animals()
{
    int i;

    Animal *p1 = create_dog("Hamlet");
    Animal *p2 = create_cat("Ofelija");
    Animal *p3 = create_dog("Polonije");

    animal_print_greeting(p1);
    animal_print_greeting(p2);
    animal_print_greeting(p3);

    animal_print_menu(p1);
    animal_print_menu(p2);
    animal_print_menu(p3);

    free(p1);
    free(p2);
    free(p3);
}

Animal stack_alloc_cat(char const* name)
{
    Animal cat;
    construct_cat(name, &cat);
    return cat;
}

Animal *create_n_dogs(int n)
{
    int i = 0;

    Animal *animal_array = (Animal *) malloc(n * sizeof(Animal));
    for (i = 0; i < n; i++) {
        construct_dog("Pas", animal_array + i);
    }

    return  animal_array;
}