#include <iostream>

class B
{
public:
    virtual int prva() = 0;
    virtual int druga() = 0;
};

class D : public B
{
public:
    virtual int prva() { return 0; }
    virtual int druga() { return 42; }
};

int main()
{
    using fun_ptr = int(*)();

    B* d = new D;
    fun_ptr* vtable = (fun_ptr*) ((fun_ptr*) d)[0];
    std::cout << vtable[0]() << std::endl << vtable[1]() << std::endl;
    delete d;

    system("PAUSE");
}