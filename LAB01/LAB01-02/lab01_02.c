#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
    double(*value_at)(void* uf, double x);
    double(*negative_value_at)(void* uf, double x);
} UnaryFunction_VTable;

typedef struct 
{
    UnaryFunction_VTable* vtable;
    int lower_bound;
    int upper_bound;
} UnaryFunction;

void UnaryFunction_construct(int lower_bound, int upper_bound, UnaryFunction* uf);
double UnaryFunction_negative_value_at(void* uf, double x);
void UnaryFunction_tabulate(UnaryFunction* uf);
bool UnaryFunction_same_functions_for_ints(UnaryFunction* uf1, UnaryFunction* uf2, double tolerance);

UnaryFunction_VTable uf_vtable = {.value_at = NULL, .negative_value_at = UnaryFunction_negative_value_at};

typedef UnaryFunction_VTable Square_VTable;

typedef struct
{
    Square_VTable* vtable;
    int lower_bound;
    int upper_bound;
} Square;

void Square_construct(int lower_bound, int upper_bound, Square* sq);
double Square_value_at(void* sq, double x);

Square_VTable sq_vtable = {.value_at = Square_value_at,.negative_value_at = UnaryFunction_negative_value_at};

Square* new_square(int lower_bound, int upper_bound);

typedef UnaryFunction_VTable Linear_VTable;

typedef struct
{
    Linear_VTable* vtable;
    int lower_bound;
    int upper_bound;
    double a_coef;
    double b_coef;
} Linear;

void Linear_construct(int lower_bound, int upper_bound, double a_coef, double b_coef, Linear* ln);
double Linear_value_at(void* ln, double x);

Linear_VTable ln_vtable = {.value_at = Linear_value_at,.negative_value_at = UnaryFunction_negative_value_at};

Linear* new_linear(int lower_bound, int upper_bound, double a_coef, double b_coef);

double value_at(UnaryFunction* uf, double x);
double negative_value_at(UnaryFunction* uf, double x);
void tabulate(UnaryFunction* uf);
bool same_function_for_ints(UnaryFunction* uf1, UnaryFunction* uf2, double tolerance);

int main()
{
    UnaryFunction* f1 = (UnaryFunction*) new_square(-2, 2);
    tabulate(f1);
    UnaryFunction* f2 = (UnaryFunction*) new_linear(-2, 2, 5, -2);
    tabulate(f2);
    printf("f1==f2: %s\n", same_function_for_ints(f1, f2, 1E-6) ? "DA" : "NE");
    printf("neg_val f2(1) = %lf\n", negative_value_at(f2, 1.0));

    printf("%d %d %d\n", sizeof(UnaryFunction), sizeof(Square), sizeof(Linear));

    free(f1);
    free(f2);

    system("PAUSE");

    return EXIT_SUCCESS;
}

void UnaryFunction_construct(int lower_bound, int upper_bound, UnaryFunction* uf)
{
    uf->lower_bound = lower_bound;
    uf->upper_bound = upper_bound;
    uf->vtable = &uf_vtable;
}

double UnaryFunction_negative_value_at(void* uf, double x)
{
    return -value_at((UnaryFunction*) uf, x);
}

void UnaryFunction_tabulate(UnaryFunction* uf)
{
    int x;
    for (x = uf->lower_bound; x <= uf->upper_bound; ++x) {
        printf("f(%d)=%lf\n", x, value_at(uf, x));
    }
}

bool UnaryFunction_same_functions_for_ints(UnaryFunction* uf1, UnaryFunction* uf2, double tolerance)
{
    int x;

    if (uf1->lower_bound != uf2->lower_bound) { return false; }
    if (uf1->upper_bound != uf2->upper_bound) { return false; }

    for (x = uf1->lower_bound; x <= uf2->upper_bound; ++x) {
        double delta = uf1->vtable->value_at(uf1, x) - uf2->vtable->value_at(uf2, x);
        if (delta < 0) delta *= -1;
        if (delta > tolerance) return false;
    }

    return true;
}

void Square_construct(int lower_bound, int upper_bound, Square* sq)
{
    UnaryFunction_construct(lower_bound, upper_bound, (UnaryFunction*) sq);
    sq->vtable = &sq_vtable;
}

double Square_value_at(void* sq, double x)
{
    return x * x;
}

Square* new_square(int lower_bound, int upper_bound)
{
    Square* square = (Square*) malloc(sizeof(Square));
    Square_construct(lower_bound, upper_bound, square);
    return square;
}

void Linear_construct(int lower_bound, int upper_bound, double a_coef, double b_coef, Linear* ln)
{
    UnaryFunction_construct(lower_bound, upper_bound, (UnaryFunction*) ln);
    ln->a_coef = a_coef;
    ln->b_coef = b_coef;
    ln->vtable = &ln_vtable;
}

double Linear_value_at(void* ln, double x)
{
    Linear* ln_ = (Linear*) ln;
    return ln_->a_coef * x + ln_->b_coef;
}

Linear* new_linear(int lower_bound, int upper_bound, double a_coef, double b_coef)
{
    Linear* linear = (Linear*) malloc(sizeof(Linear));
    Linear_construct(lower_bound, upper_bound, a_coef, b_coef, linear);
    return linear;
}

double value_at(UnaryFunction* uf, double x)
{
    return uf->vtable->value_at(uf, x);
}

double negative_value_at(UnaryFunction* uf, double x)
{
    return uf->vtable->negative_value_at(uf, x);
}

void tabulate(UnaryFunction* uf)
{
    UnaryFunction_tabulate(uf);
}

bool same_function_for_ints(UnaryFunction* uf1, UnaryFunction* uf2, double tolerance)
{
    return UnaryFunction_same_functions_for_ints(uf1, uf2, tolerance);
}
