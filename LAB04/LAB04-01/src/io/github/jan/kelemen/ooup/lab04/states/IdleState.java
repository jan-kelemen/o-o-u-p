package io.github.jan.kelemen.ooup.lab04.states;

import io.github.jan.kelemen.ooup.lab04.Renderer;
import io.github.jan.kelemen.ooup.lab04.models.GraphicalObject;
import io.github.jan.kelemen.ooup.lab04.models.Point;

public class IdleState implements State {
    @Override
    public void mouseDown(final Point point, final boolean shiftDown, final boolean ctrlDown) {

    }

    @Override
    public void mouseUp(final Point mousePoint, final boolean shiftDown, final boolean ctrlDown) {

    }

    @Override
    public void mouseDragged(final Point mousePoint) {

    }

    @Override
    public void keyPressed(final int keyCode) {

    }

    @Override
    public void afterDraw(final Renderer r, final GraphicalObject go) {

    }

    @Override
    public void afterDraw(final Renderer r) {

    }

    @Override
    public void onLeaving() {

    }
}
