package io.github.jan.kelemen.ooup.lab04.models;

import io.github.jan.kelemen.ooup.lab04.GeometryUtil;

import java.util.*;

public abstract class AbstractGraphicalObject implements GraphicalObject {

    private Point[] hotPoints;
    private boolean[] hotPointSelected;
    private boolean selected;
    private List<GraphicalObjectListener> listeners;

    private AbstractGraphicalObject() {
        super();
        listeners = new ArrayList<>();
        this.selected = false;
    }

    public AbstractGraphicalObject(Point[] hotPoints) {
        this();

        Objects.requireNonNull(hotPoints);

        this.hotPoints = Arrays.copyOf(hotPoints, hotPoints.length);
        this.hotPointSelected = new boolean[hotPoints.length];
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(final boolean selected) {
        this.selected = selected;
        notifySelectionListeners();
    }

    @Override
    public int getNumberOfHotPoints() {
        return hotPoints.length;
    }

    @Override
    public Point getHotPoint(final int index) {
        return hotPoints[index];
    }

    @Override
    public void setHotPoint(final int index, final Point point) {
        hotPoints[index] = point;
        notifyListeners();
    }

    @Override
    public boolean isHotPointSelected(final int index) {
        return hotPointSelected[index];
    }

    @Override
    public void setHotPointSelected(final int index, final boolean selected) {
        hotPointSelected[index] = selected;
        notifySelectionListeners();
    }

    @Override
    public double getHotPointDistance(final int index, final Point mousePoint) {
        return GeometryUtil.distanceFromPoint(hotPoints[index], mousePoint);
    }

    @Override
    public void translate(final Point delta) {
        for(int i = 0; i < hotPoints.length; i++) {
            hotPoints[i] = hotPoints[i].translate(delta);
        }
        notifyListeners();
    }

    @Override
    public void addGraphicalObjectListener(final GraphicalObjectListener l) {
        listeners.add(l);
    }

    @Override
    public void removeGraphicalObjectListener(final GraphicalObjectListener l) {
        listeners.remove(l);
    }

    public Point[] getHotPoints() {
        return hotPoints;
    }

    private void notifyListeners() {
        for (GraphicalObjectListener l : listeners) {
            l.graphicalObjectChanged(this);
        }
    }

    private void notifySelectionListeners() {
        for(GraphicalObjectListener l : listeners) {
            l.graphicalObjectSelectionChanged(this);
        }
    }
}
