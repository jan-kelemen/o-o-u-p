package io.github.jan.kelemen.ooup.lab04.models;

import io.github.jan.kelemen.ooup.lab04.Renderer;

import java.util.List;
import java.util.Stack;

public class Oval extends AbstractGraphicalObject {

    private Point center;

    private int APPROX_DRAWING_POINTS = 256;

    public Oval(Point bottom, Point right) {
        super(new Point[] {bottom, right});

        center = new Point(bottom.getX(), right.getY());
    }

    public Oval() {
        this(new Point(200, 400), new Point(500, 200));
    }

    @Override
    public Rectangle getBoundingBox() {
        return null;
    }

    @Override
    public double selectionDistance(final Point mousePoint) {
        return 0;
    }

    @Override
    public void render(final Renderer r) {
        r.fillPolygon(getPoints());
    }

    @Override
    public String getShapeName() {
        return "Oval";
    }

    @Override
    public GraphicalObject duplicate() {
        Point[] points = getHotPoints();

        Oval copy = new Oval(points[0], points[1]);
        copy.setSelected(isSelected());
        copy.setHotPointSelected(0, isHotPointSelected(0));
        copy.setHotPointSelected(1, isHotPointSelected(1));

        return copy;
    }

    @Override
    public String getShapeID() {
        return null;
    }

    @Override
    public void load(final Stack<GraphicalObject> stack, final String data) {

    }

    @Override
    public void save(final List<String> rows) {

    }

    private Point[] getPoints() {
        Point bottom = getHotPoint(0);
        Point right = getHotPoint(1);
        int a = right.getX() - bottom.getX();
        int b = bottom.getY() - right.getX();

        Point[] points = new Point[APPROX_DRAWING_POINTS];
        for (int i = 0; i < APPROX_DRAWING_POINTS; i++) {
            double t = (2*Math.PI/APPROX_DRAWING_POINTS) * i;
            int x = (int)(a * Math.cos(t)) + center.getX();
            int y = (int)(b * Math.sin(t)) + center.getY();
            points[i] = new Point(x, y);
        }
        return points;
    }
}
