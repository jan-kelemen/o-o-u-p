package io.github.jan.kelemen.ooup.lab04;

import io.github.jan.kelemen.ooup.lab04.models.Point;

public interface Renderer {

    void drawLine(Point s, Point e);

    void fillPolygon(Point[] points);
}
