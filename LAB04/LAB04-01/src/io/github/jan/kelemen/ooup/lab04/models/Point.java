package io.github.jan.kelemen.ooup.lab04.models;

import java.util.Objects;

public class Point implements Comparable<Point>{

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point translate(Point dp) {
        Objects.requireNonNull(dp);
        return new Point(x + dp.getX(), y + dp.getY());
    }

    public Point difference(Point p) {
        Objects.requireNonNull(p);
        return  new Point(Math.abs(x - p.getX()), Math.abs(y - p.getY()));
    }

    @Override
    public int compareTo(final Point o) {
        if(x < o.getX()) {
            return -1;
        }

        if(x == o.getX()) {
            if(y < o.getY()) {
                return -1;
            }

            return y == o.getY() ? 0 : 1;
        }

        return 1;
    }
}
