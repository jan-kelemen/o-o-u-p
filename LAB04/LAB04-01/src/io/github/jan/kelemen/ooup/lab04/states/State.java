package io.github.jan.kelemen.ooup.lab04.states;

import io.github.jan.kelemen.ooup.lab04.Renderer;
import io.github.jan.kelemen.ooup.lab04.models.GraphicalObject;
import io.github.jan.kelemen.ooup.lab04.models.Point;

public interface State {

    void mouseDown(Point point, boolean shiftDown, boolean ctrlDown);

    void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown);

    void mouseDragged(Point mousePoint);

    void keyPressed(int keyCode);

    void afterDraw(Renderer r, GraphicalObject go);

    void afterDraw(Renderer r);

    void onLeaving();
}
