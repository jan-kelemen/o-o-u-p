package io.github.jan.kelemen.ooup.lab04.models;

public interface DocumentModelListener {

    void documentChange();
}
