package io.github.jan.kelemen.ooup.lab04.models;

import io.github.jan.kelemen.ooup.lab04.GeometryUtil;
import io.github.jan.kelemen.ooup.lab04.Renderer;

import java.util.List;
import java.util.Stack;

public class LineSegment extends AbstractGraphicalObject {

    public LineSegment(Point start, Point end) {
        super(new Point[] {start, end});
    }

    public LineSegment() {
        this(new Point(10,10), new Point(10, 100));
    }

    @Override
    public Rectangle getBoundingBox() {
        Point[] points = getHotPoints();
        Point first = points[0];
        Point second = points[1];

        if(first.compareTo(second) >= 0) {
            Point temp = first;
            first = second;
            second = temp;
        }

        Point difference = first.difference(second);

        return new Rectangle(first.getX(), first.getY(), difference.getX(), difference.getY());
    }

    @Override
    public double selectionDistance(final Point mousePoint) {
        Point[] points = getHotPoints();
        return GeometryUtil.distanceFromLineSegment(points[0], points[1], mousePoint);
    }

    @Override
    public void render(final Renderer r) {
        r.drawLine(getHotPoint(0), getHotPoint(1));
    }

    @Override
    public String getShapeName() {
        return "Line";
    }

    @Override
    public GraphicalObject duplicate() {
        Point[] points = getHotPoints();

        LineSegment copy = new LineSegment(points[0], points[1]);
        copy.setSelected(isSelected());
        copy.setHotPointSelected(0, isHotPointSelected(0));
        copy.setHotPointSelected(1, isHotPointSelected(1));

        return copy;
    }

    @Override
    public String getShapeID() {
        return null;
    }

    @Override
    public void load(final Stack<GraphicalObject> stack, final String data) {

    }

    @Override
    public void save(final List<String> rows) {
    }
}
