package io.github.jan.kelemen.ooup.lab04.graphics;

import io.github.jan.kelemen.ooup.lab04.*;
import io.github.jan.kelemen.ooup.lab04.models.Point;

import java.awt.*;

public class G2DRendererImpl implements Renderer {

    private Graphics2D g2d;

    public G2DRendererImpl(Graphics2D g2d) {
        this.g2d = g2d;
    }

    @Override
    public void drawLine(final Point s, final Point e) {
        g2d.setColor(Color.BLUE);
        g2d.drawLine(s.getX(), s.getY(), e.getX(), e.getY());
    }

    @Override
    public void fillPolygon(final Point[] points) {
        g2d.setColor(Color.BLUE);
        int[] xPoints = new int[points.length];
        int[] yPoints = new int[points.length];
        for (int i = 0; i < points.length; i++) {
            xPoints[i] = points[i].getX();
            yPoints[i] = points[i].getY();
        }
        g2d.fillPolygon(xPoints, yPoints, points.length);

        g2d.setColor(Color.RED);
        g2d.drawPolygon(xPoints, yPoints, points.length);
    }
}
