package io.github.jan.kelemen.ooup.lab04.models;

import io.github.jan.kelemen.ooup.lab04.models.GraphicalObject;

public interface GraphicalObjectListener {

    void graphicalObjectChanged(GraphicalObject go);

    void graphicalObjectSelectionChanged(GraphicalObject go);
}
