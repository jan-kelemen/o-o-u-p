package io.github.jan.kelemen.ooup.lab04;

import io.github.jan.kelemen.ooup.lab04.models.Point;

public class GeometryUtil {

    public static double distanceFromPoint(Point p1, Point p2) {
        return Math.hypot(p1.getX() - p2.getX(), p1.getY() - p2.getY());
    }

    public static double distanceFromLineSegment(Point s, Point e, Point p) {
        return Math.abs((e.getY() - s.getY()) * p.getX() - (e.getX() - s.getX()) * p.getY() + e.getX() * s.getY() -
                        e.getY() * s.getX()) / distanceFromPoint(s, e);
    }
}
