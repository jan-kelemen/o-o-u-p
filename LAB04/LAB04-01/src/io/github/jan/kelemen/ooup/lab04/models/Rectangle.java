package io.github.jan.kelemen.ooup.lab04.models;

/**
 * <code>Rectangle</code> .
 *
 * @author Jan Kelemen
 * @version alpha
 */
public class Rectangle {

    private int x;
    private int y;
    private int width;
    private int height;

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
