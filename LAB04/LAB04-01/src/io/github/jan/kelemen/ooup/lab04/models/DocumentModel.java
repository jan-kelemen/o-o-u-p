package io.github.jan.kelemen.ooup.lab04.models;

import io.github.jan.kelemen.ooup.lab04.GeometryUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class DocumentModel {

    public static final double SELECTION_PROXIMITY = 10;

    private List<GraphicalObject> objects;

    private List<GraphicalObject> readOnlyObjects;

    private List<DocumentModelListener> listeners;

    private List<GraphicalObject> selectedObjects;

    private List<GraphicalObject> readOnlySelectedObjects;

    private final GraphicalObjectListener goListener = new GraphicalObjectListener() {
        @Override
        public void graphicalObjectChanged(final GraphicalObject go) {

        }

        @Override
        public void graphicalObjectSelectionChanged(final GraphicalObject go) {

        }
    };

    public DocumentModel() {
        objects = new ArrayList<>();
        readOnlyObjects = Collections.unmodifiableList(objects);
        listeners = new ArrayList<>();
        selectedObjects = new ArrayList<>();
        readOnlySelectedObjects = Collections.unmodifiableList(selectedObjects);
    }

    public void clear() {

    }

    public void addGraphicalObject(GraphicalObject obj) {
        Objects.requireNonNull(obj);

        objects.add(obj);
        readOnlyObjects = Collections.unmodifiableList(objects);
        if(obj.isSelected()) {
            selectedObjects.add(obj);
            readOnlySelectedObjects = Collections.unmodifiableList(selectedObjects);
        }

        obj.addGraphicalObjectListener(goListener);

        notifiyListeners();
    }

    public void removeGraphicalObject(GraphicalObject obj) {
        Objects.requireNonNull(obj);

        objects.remove(obj);
        readOnlyObjects = Collections.unmodifiableList(objects);
        selectedObjects.remove(obj);
        readOnlyObjects = Collections.unmodifiableList((selectedObjects));

        obj.removeGraphicalObjectListener(goListener);

        notifiyListeners();
    }

    public List<GraphicalObject> list() {
        return readOnlyObjects;
    }

    public void addDocumentModelListener(DocumentModelListener l) {
        listeners.add(l);
    }

    public void removeDocumentModelListener(DocumentModelListener l) {
        listeners.remove(l);
    }

    public void notifiyListeners() {
        for(DocumentModelListener l : listeners) {
            l.documentChange();
        }
    }
    public List<GraphicalObject> getSelectedObjects() {
        return readOnlySelectedObjects;
    }

    public void increaseZ(GraphicalObject go) {
        int objIndex = objects.indexOf(go);
        if(objIndex < objects.size() - 1) {
            readOnlyObjects = swapAndReturnUnmodifiable(objects, objIndex, 1);
        }

        if(go.isSelected()) {
            int selIndex = selectedObjects.indexOf(go);
            if(selIndex < selectedObjects.size() - 1) {
                selectedObjects = swapAndReturnUnmodifiable(selectedObjects, selIndex, 1);
            }
        }

        notifiyListeners();
    }

    public void decreaseZ(GraphicalObject go) {
        int objIndex = objects.indexOf(go);
        if(objIndex > 0) {
            Collections.swap(objects, objIndex, objIndex - 1);
            readOnlyObjects = swapAndReturnUnmodifiable(objects, objIndex, -1);
        }

        if(go.isSelected()) {
            int selIndex = selectedObjects.indexOf(go);
            if(selIndex > 0) {
                selectedObjects = swapAndReturnUnmodifiable(selectedObjects, selIndex, -1);
            }
        }

        notifiyListeners();
    }

    private List<GraphicalObject> swapAndReturnUnmodifiable(List<GraphicalObject> list, int index, int step) {
        Collections.swap(list, index, index + step);
        return Collections.unmodifiableList(list);
    }

    public GraphicalObject findSelectedGraphicalObject(Point mousePoint) {
        for(GraphicalObject go : objects) {
            if(go.selectionDistance(mousePoint) < SELECTION_PROXIMITY) {
                return go;
            }
        }
        return null;
    }

    public int findSelectedHotPoint(GraphicalObject object, Point mousePoint) {
        for(int i = 0; i < object.getNumberOfHotPoints(); i++) {
            if(GeometryUtil.distanceFromPoint(object.getHotPoint(i), mousePoint) < SELECTION_PROXIMITY) {
                return i;
            }
        }
        return -1;
    }
}
