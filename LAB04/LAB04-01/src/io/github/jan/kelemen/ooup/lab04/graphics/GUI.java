package io.github.jan.kelemen.ooup.lab04.graphics;

import io.github.jan.kelemen.ooup.lab04.Renderer;
import io.github.jan.kelemen.ooup.lab04.models.*;
import io.github.jan.kelemen.ooup.lab04.models.Point;
import io.github.jan.kelemen.ooup.lab04.states.AddShapeState;
import io.github.jan.kelemen.ooup.lab04.states.IdleState;
import io.github.jan.kelemen.ooup.lab04.states.State;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Objects;

public class GUI extends JFrame {

    private class Canvas extends JComponent {

        public Canvas() {
            super();
            setBackground(Color.WHITE);
            setFocusable(true);
            requestFocusInWindow();
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D)g;
            Renderer r = new G2DRendererImpl(g2d);
            for(GraphicalObject o : objects) {
                o.render(r);
                currentState.afterDraw(r, o);
            }
            currentState.afterDraw(r);
        }
    }

    private class CanvasAction extends AbstractAction {

        private GraphicalObject go;

        public CanvasAction(GraphicalObject go) {
            this.go = go;
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            currentState.onLeaving();
            currentState = new AddShapeState(documentModel, go);
        }
    }

    private class LoadAction extends AbstractAction {

        @Override
        public void actionPerformed(final ActionEvent e) {

        }
    }

    private class SaveAction extends AbstractAction {

        @Override
        public void actionPerformed(final ActionEvent e) {

        }
    }

    private class ExportAction extends AbstractAction {

        @Override
        public void actionPerformed(final ActionEvent e) {

        }
    }

    private class SelectAction extends AbstractAction {

        @Override
        public void actionPerformed(final ActionEvent e) {

        }
    }

    private class EraseAction extends AbstractAction {

        @Override
        public void actionPerformed(final ActionEvent e) {

        }
    }

    private List<GraphicalObject> objects;
    private State currentState;
    private DocumentModel documentModel;
    private Canvas canvas;

    public GUI() {
        currentState = new IdleState();
        documentModel = new DocumentModel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Paint.NOT!");

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        // get 2/3 of the height, and 2/3 of the width
        int height = screenSize.height * 2 / 3;
        int width = screenSize.width * 2 / 3;

        // set the jframe height and width
        setSize(width, height);
    }

    public GUI(List<GraphicalObject> objects) {
        this();
        Objects.requireNonNull(objects);
        this.objects = objects;

        initGUI();

        documentModel.addGraphicalObject(objects.get(0));
        documentModel.addGraphicalObject(objects.get(1));
    }

    private void initGUI() {
        addToolbar();
        addCanvas();

        documentModel.addDocumentModelListener(() -> canvas.repaint());
    }

    private void addToolbar() {
        JToolBar toolbar = new JToolBar();

        addAction(toolbar, "Učitaj", new LoadAction());
        addAction(toolbar, "Pohrani", new SaveAction());
        addAction(toolbar, "SVG Export", new ExportAction());

        for(GraphicalObject go : objects) {
            addAction(toolbar, go.getShapeName(), new CanvasAction(go));
        }

        addAction(toolbar, "Selektiraj", new SelectAction());
        addAction(toolbar, "Brisalo", new EraseAction());

        add(toolbar, BorderLayout.NORTH);
    }

    private void addAction(JToolBar toolBar, String display, Action action) {
        action.putValue(Action.NAME, display);
        toolBar.add(action);
    }

    private void addCanvas() {
        Canvas canvas = new Canvas();

        add(canvas, BorderLayout.CENTER);

        canvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    currentState.onLeaving();
                    currentState = new IdleState();
                } else {
                    currentState.keyPressed(e.getKeyCode());
                }
            }
        });

        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point point = new Point(e.getX(), e.getY());
                currentState.mouseDown(point, e.isShiftDown(), e.isControlDown());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Point point = new Point(e.getX(), e.getY());
                currentState.mouseUp(point, e.isShiftDown(), e.isControlDown());
            }
        });

        canvas.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                Point point = new Point(e.getX(), e.getY());
                currentState.mouseDragged(point);
            }
        });

        this.canvas = canvas;
    }
}
