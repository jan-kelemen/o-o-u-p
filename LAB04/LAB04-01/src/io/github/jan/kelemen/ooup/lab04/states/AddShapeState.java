package io.github.jan.kelemen.ooup.lab04.states;

import io.github.jan.kelemen.ooup.lab04.Renderer;
import io.github.jan.kelemen.ooup.lab04.models.DocumentModel;
import io.github.jan.kelemen.ooup.lab04.models.GraphicalObject;
import io.github.jan.kelemen.ooup.lab04.models.Point;

public class AddShapeState implements State {

    private GraphicalObject prototype;
    private DocumentModel documentModel;

    public AddShapeState(DocumentModel documentModel, GraphicalObject prototype) {
        this.documentModel = documentModel;
        this.prototype = prototype;
    }

    @Override
    public void mouseDown(final Point point, final boolean shiftDown, final boolean ctrlDown) {
        GraphicalObject newPrototype = prototype.duplicate();
        newPrototype.translate(point);
        documentModel.addGraphicalObject(newPrototype);
    }

    @Override
    public void mouseUp(final Point mousePoint, final boolean shiftDown, final boolean ctrlDown) {

    }

    @Override
    public void mouseDragged(final Point mousePoint) {

    }

    @Override
    public void keyPressed(final int keyCode) {

    }

    @Override
    public void afterDraw(final Renderer r, final GraphicalObject go) {

    }

    @Override
    public void afterDraw(final Renderer r) {

    }

    @Override
    public void onLeaving() {

    }
}
