package io.github.jan.kelemen.ooup.lab04;

import io.github.jan.kelemen.ooup.lab04.graphics.GUI;
import io.github.jan.kelemen.ooup.lab04.models.GraphicalObject;
import io.github.jan.kelemen.ooup.lab04.models.LineSegment;
import io.github.jan.kelemen.ooup.lab04.models.Oval;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <code>PaintDotNot</code> .
 *
 * @author Jan Kelemen
 * @version alpha
 */
public class PaintDotNot {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                List<GraphicalObject> objects = new ArrayList<>();

                objects.add(new LineSegment());
                objects.add(new Oval());

                GUI gui = new GUI(objects);
                gui.setVisible(true);
            }
        });
    }
}
