#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

struct Point
{
    int x;
    int y;
};

struct Shape
{
    virtual void draw() = 0;
    virtual void move(int x, int y) = 0;

    virtual ~Shape() = default;
};

struct Circle : public Shape
{
    virtual void draw() override { std::cerr << "in Circle::draw()\n"; }
    virtual void move(int x, int y) override { std::cerr << "in Circle::move()\n"; }

    virtual ~Circle() = default;
private:
    double radius_;
    Point center_;
};

struct Square : public Shape
{
    virtual void draw() override { std::cerr << "in Square::draw()\n"; }
    virtual void move(int x, int y) override { std::cerr << "in Square:::move()\n"; }

    virtual ~Square() = default;
private:
    double side;
    Point center_;
};

struct Rhomb : public Shape
{
    virtual void draw() override { std::cerr << "in Rhomb::draw()\n"; }
    virtual void move(int x, int y) override { std::cerr << "in Rhomb::move()\n"; }

    virtual ~Rhomb() = default;
};

int main()
{
    auto shapes = std::vector<std::unique_ptr<Shape>>();
    shapes.push_back(std::make_unique<Circle>());
    shapes.push_back(std::make_unique<Square>());
    shapes.push_back(std::make_unique<Square>());
    shapes.push_back(std::make_unique<Circle>());
    shapes.push_back(std::make_unique<Rhomb>());

    std::for_each(shapes.begin(), shapes.end(), [](std::unique_ptr<Shape>& shape) { shape->draw(); });

    std::for_each(shapes.begin(), shapes.end(), [](std::unique_ptr<Shape>& shape) { shape->move(0, 1); });

    system("pause");
};