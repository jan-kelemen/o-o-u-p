#ifndef LOG_MY_DBASE_H

#define LOG_MY_DBASE_H

#include <memory>

#include "MyDBase.h"

class LogMyDBase : public MyDBase
{
public:
    LogMyDBase(std::shared_ptr<MyDBase> db);

    virtual int query(Param const& param) override;

    virtual ~LogMyDBase() = default;
private:
    std::shared_ptr<MyDBase> db_;
};
#endif // LOG_MY_DBASE_H