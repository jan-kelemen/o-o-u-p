#include <exception>

#include "ExceptionMyDBase.h"

ExceptionMyDBase::ExceptionMyDBase(std::shared_ptr<MyDBase> db) : db_{db}
{
    ;
}

int ExceptionMyDBase::query(Param const& param)
{
    if (db_->query(param)) {
        throw std::logic_error(param.table + " | " + param.column + " | " + param.key);
    }
    
    return 0;
}
