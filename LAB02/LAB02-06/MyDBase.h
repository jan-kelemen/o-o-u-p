#ifndef MY_DBASE_H

#define MY_DBASE_H

#include <string>

class MyDBase
{
public:
    struct Param
    {
        std::string table;
        std::string column;
        std::string key;
    };

    virtual int query(Param const& param) = 0;

    virtual ~MyDBase() = default;
};

inline bool operator==(MyDBase::Param const& rhs, MyDBase::Param const& lhs)
{
    return rhs.table == lhs.table && rhs.column == lhs.column && rhs.key == lhs.key;
}

#endif // !MY_DBASE_H