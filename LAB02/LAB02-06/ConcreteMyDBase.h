#ifndef CONCRETE_MY_DBASE_H

#define CONCRETE_MY_DBASE_H

#include <vector>

#include "MyDBase.h"

class ConcreteMyDBase : public MyDBase
{
public:
    ConcreteMyDBase();

    virtual int query(MyDBase::Param const& param) override;

    virtual ~ConcreteMyDBase() = default;
private:
    std::vector<MyDBase::Param> db_data_;
};
#endif // !CONCRETE_MY_DBASE_H