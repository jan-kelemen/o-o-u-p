#ifndef EXCEPTION_MY_DBASE_H

#define EXCEPTION_MY_DBASE_H

#include <memory>

#include "MyDBase.h"

class ExceptionMyDBase : public MyDBase
{
public:
    ExceptionMyDBase(std::shared_ptr<MyDBase> db);

    virtual int query(Param const& param) override;

    virtual ~ExceptionMyDBase() = default;
private:
    std::shared_ptr<MyDBase> db_;
};
#endif // !EXCEPTION_MY_DBASE_H