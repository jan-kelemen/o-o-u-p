#include <iostream>
#include <memory>

#include "MyDBase.h"
#include "ConcreteMyDBase.h"
#include "LogMyDBase.h"
#include "ExceptionMyDBase.h"

int run_query(MyDBase* db, MyDBase::Param const& p);

int main()
{
    std::shared_ptr<MyDBase> db = std::make_shared<ConcreteMyDBase>();
    std::shared_ptr<MyDBase> log_db = std::make_shared<LogMyDBase>(db);
    std::shared_ptr<MyDBase> exc_db = std::make_shared<ExceptionMyDBase>(db);
    std::shared_ptr<MyDBase> exc_log_db = std::make_shared<ExceptionMyDBase>(log_db);

    std::cout << run_query(db.get(), {"a", "b", "c"}) << "\n";
    std::cout << run_query(log_db.get(), {"tbl1", "col1", "key1"}) << "\n";
    std::cout << run_query(exc_db.get(), {"tbl1", "col3", "key5"}) << "\n";
    std::cout << run_query(exc_log_db.get(), {"tbl2", "col2", "key3"}) << "\n";
    std::cout << std::flush;

    system("pause");
}

int run_query(MyDBase* db, MyDBase::Param const& p)
{
    try {
        return db->query(p);
    }
    catch (std::exception& e) {
        std::cout << e.what() << std::endl;
        return 1;
    }
}
