#include <iostream>
#include <fstream>

#include "LogMyDBase.h"

LogMyDBase::LogMyDBase(std::shared_ptr<MyDBase> db) : db_{db}
{
    ;
}

int LogMyDBase::query(Param const& param)
{
    auto stream = std::ofstream("mydbase.log", std::fstream::app);
    stream << param.table << " | " << param.column << " | " << param.table << std::endl;
    stream.close();

    return db_->query(param);
}
