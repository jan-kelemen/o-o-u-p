#include <algorithm>

#include "ConcreteMyDBase.h"

ConcreteMyDBase::ConcreteMyDBase()
    : db_data_{{"tbl1", "col1", "key1"}, {"tbl1", "col1", "key2"}, {"tbl2", "col2", "key3"}}
{
}

int ConcreteMyDBase::query(MyDBase::Param const& param)
{
    return std::find(db_data_.cbegin(), db_data_.cend(), param) == db_data_.cend();
}
