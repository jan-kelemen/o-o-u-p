#include <iostream>
#include <vector>

struct Point
{
    int x;
    int y;
};

struct Shape
{
    enum EType { circle, square, rhomb };
    EType type_;
};

struct Circle
{
    Shape::EType type_;
    double radius_;
    Point center_;
};

struct Square
{
    Shape::EType type_;
    double side_;
    Point center_;
};

struct Rhomb
{
    Shape::EType type_;
    int random_;
};

void draw_square(Square const*) { std::cerr << "in draw_square\n"; }

void draw_circle(Circle const*) { std::cerr << "in draw_circle\n"; }

void draw_rhomb(Rhomb const*) { std::cerr << "in draw_rhomb\n"; }

void draw_shapes(std::vector<Shape*> const& shapes)
{
    for (const auto& shape : shapes) {
        switch (shape->type_) {
            case Shape::square:
                draw_square(reinterpret_cast<Square const*>(shape));
                break;
            case Shape::circle:
                draw_circle(reinterpret_cast<Circle const*>(shape));
                break;
            case Shape::rhomb:
                draw_rhomb(reinterpret_cast<Rhomb const*>(shape));
                break;
            default:
                exit(1);
                break;
        }
    }
}

void move_square(Square* square, int x, int y) { std::cerr << "in move_square\n"; }

void move_circle(Circle* circle, int x, int y) { std::cerr << "in move_circle\n"; }

void move_rhomb(Rhomb* rhomb, int x, int y) { std::cerr << "in move_rhomb\n"; }

void move_shapes(std::vector<Shape*> const& shapes, int x, int y)
{
    for (const auto& shape : shapes) {
        switch (shape->type_) {
            case Shape::square:
                move_square(reinterpret_cast<Square*>(shape), x, y);
                break;
            case Shape::circle:
                move_circle(reinterpret_cast<Circle*>(shape), x, y);
                break;
            case Shape::rhomb:
                move_rhomb(reinterpret_cast<Rhomb*>(shape), x, y);
                break;
            default:
                exit(1);
                break;
        }
    }
}

int main()
{
    auto shapes = std::vector<Shape*>(5);

    shapes[0] = reinterpret_cast<Shape*>(new Circle);
    shapes[0]->type_ = Shape::circle;
    shapes[1] = reinterpret_cast<Shape*>(new Square);
    shapes[1]->type_ = Shape::square;
    shapes[2] = reinterpret_cast<Shape*>(new Square);
    shapes[2]->type_ = Shape::square;
    shapes[3] = reinterpret_cast<Shape*>(new Circle);
    shapes[3]->type_ = Shape::circle;
    shapes[4] = reinterpret_cast<Shape*>(new Rhomb);
    shapes[4]->type_ = Shape::rhomb;

    draw_shapes(shapes);

    move_shapes(shapes, 0, 1);

    for (auto& ptr : shapes) {
        switch (ptr->type_) {
            case Shape::square:
                delete reinterpret_cast<Square*>(ptr);
                break;
            case Shape::circle:
                delete reinterpret_cast<Circle*>(ptr);
                break;
            case Shape::rhomb:
                delete reinterpret_cast<Rhomb*>(ptr);
                break;
            default:
                exit(1);
                break;
        }
    }

    system("pause");
}