#ifndef PERCENTILE_CALCULATOR_H

#define PERCENTILE_CALCULATOR_H

#include <vector>

class PercentileCalculator
{
public:
    virtual int calculate(std::vector<int> const& sequence, int percentile) const = 0;

    virtual ~PercentileCalculator() = default;
};

#endif // !PERCENTILE_CALCULATOR_H