#ifndef NEAREST_PERCENTILE_CALCULATOR_H

#define NEAREST_PERCENTILE_CALCULATOR_H

#include "PercentileCalculator.h"

class NearestPercentileCalculator : public PercentileCalculator
{
public:
    virtual int calculate(std::vector<int> const& sequence, int percentile) const override;

    virtual ~NearestPercentileCalculator() = default;
};

#endif // !NEAREST_PERCENTILE_CALCULATOR_H