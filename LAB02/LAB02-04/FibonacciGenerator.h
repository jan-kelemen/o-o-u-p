#ifndef FIBONACCI_GENERATOR_H

#define FIBONACCI_GENERATOR_H

#include "Generator.h"

class FibonacciGenerator : public Generator
{
public:
    FibonacciGenerator(int count);

    virtual std::vector<int> generate() const override;

    virtual ~FibonacciGenerator() = default;
private:
    int count_;
};

#endif // !FIBONACCI_GENERATOR_H