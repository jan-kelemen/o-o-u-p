#ifndef DISTRIBUTION_TESTER_H

#define DISTRIBUTION_TESTER_H

#include <initializer_list>
#include <vector>

#include "Generator.h"
#include "PercentileCalculator.h"

class DistributionTester
{
public:
    DistributionTester(std::initializer_list<int> precentiles);

    void test(Generator const& generator, PercentileCalculator const& calculator) const;
private:
    std::vector<int> percentiles_;
};
#endif // !DISTRIBUTION_TESTER_H