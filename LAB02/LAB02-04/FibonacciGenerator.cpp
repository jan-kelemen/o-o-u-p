#include "FibonacciGenerator.h"

FibonacciGenerator::FibonacciGenerator(int count)
    : count_{count}
{
    ;
}

auto fibonacci(int x)
{
    if (x == 0)
        return 0;

    if (x == 1)
        return 1;

    return fibonacci(x - 1) + fibonacci(x - 2);
}

std::vector<int> FibonacciGenerator::generate() const
{
    auto result = std::vector<int>(count_);
    for (auto i = 1; i <= count_; ++i) {
        result[i - 1] = fibonacci(i);
    }
    return result;
}
