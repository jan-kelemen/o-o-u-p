#ifndef INTERPOLATED_PERCENTILE_CALCULATOR_H

#define INTERPOLATED_PERCENTILE_CALCULATOR_H

#include "PercentileCalculator.h"

class InterpolatedPercentileCalculator : public PercentileCalculator
{
public:
    virtual int calculate(std::vector<int> const& sequence, int percentile) const override;

    virtual ~InterpolatedPercentileCalculator() = default;
};

#endif // !INTERPOLATED_PERCENTILE_CALCULATOR_H