#include <iostream>
#include <string>
#include <sstream>

#include "DistributionTester.h"

DistributionTester::DistributionTester(std::initializer_list<int> precentiles)
    : percentiles_{precentiles}
{
    ;
}

std::string print_distribution(std::vector<int> const& distribution)
{
    auto stream = std::ostringstream();
    for (const auto& value : distribution) {
        stream << value << ", ";
    }
    auto string = stream.str();

    return string.substr(0, string.length() - 2) + " : ";
}

void DistributionTester::test(Generator const& generator, PercentileCalculator const& calculator) const
{
    auto sequence = generator.generate();
    std::cout << print_distribution(sequence);
    for (const auto& percentile : percentiles_) {
        std::cout << calculator.calculate(sequence, percentile) << " | ";
    }
    std::cout << std::endl;
}
