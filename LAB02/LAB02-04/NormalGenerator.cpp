#include "NormalGenerator.h"

NormalGenerator::NormalGenerator(int mean, int stddev, int count)
    : mean_{mean},
    stddev_{stddev},
    count_{count},
    gen_{std::random_device()()}
{
    ;
}

std::vector<int> NormalGenerator::generate() const
{
    auto distribution = std::normal_distribution<>(mean_, stddev_);
    auto result = std::vector<int>(count_);
    for (auto i = 0; i < count_; ++i) {
        result[i] = std::round(distribution(gen_));
    }
    return result;
}
