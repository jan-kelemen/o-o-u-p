#ifndef SEQUENTIAL_GENERATOR_H

#define SEQUENTIAL_DISTRITUBITON_H

#include "GENERATOR.h"

class SequentialGenerator : public Generator
{
public:
    SequentialGenerator(int lower_bound, int upper_bound, int step);

    virtual std::vector<int> generate() const override;

    virtual ~SequentialGenerator() = default;
private:
    int lower_bound_;
    int upper_bound_;
    int step_;
};

#endif // !SEQUENTIAL_GENERATOR_H