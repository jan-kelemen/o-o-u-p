#include <algorithm>

#include "InterpolatedPercentileCalculator.h"

int InterpolatedPercentileCalculator::calculate(std::vector<int> const& sequence, int percentile) const
{
    auto sequence_ = sequence;
    std::sort(sequence_.begin(), sequence_.end());

    auto size = sequence_.size();
    if (percentile < 100 * (1 - 0.5) / size) { return sequence_[0]; }
    if (percentile > 100 * (size - 0.5) / size) { return sequence_[size - 1]; }

    for (auto i = 0; i < size; ++i) {
        auto lower = 100 * (i + 1 - 0.5) / size;
        auto upper = 100 * (i + 1 + 1 - 0.5) / size;
        if (percentile >= lower && percentile <= upper) {
            return sequence_[i] + size * (percentile - lower) * (sequence_[i + 1] - sequence_[i]) / 100;
        }
    }

    return -1;
}
