#include <algorithm>

#include "NearestPercentileCalculator.h"

auto percentile_index(int percentile, int size)
{
    return static_cast<int>(std::round(static_cast<double>(percentile * size) / 100 + 0.5)) - 1;
}

int NearestPercentileCalculator::calculate(std::vector<int> const& sequence, int percentile) const
{
    auto sequence_ = sequence;
    std::sort(sequence_.begin(), sequence_.end());

    auto position = percentile_index(percentile, sequence_.size());

    return sequence_[position];
}
