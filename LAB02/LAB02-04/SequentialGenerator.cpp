#include "SequentialGenerator.h"

SequentialGenerator::SequentialGenerator(int lower_bound, int upper_bound, int step)
    : lower_bound_{lower_bound},
    upper_bound_{upper_bound},
    step_{step}
{
    ;
}

std::vector<int> SequentialGenerator::generate() const
{
    auto result = std::vector<int>();
    for (auto i = lower_bound_; i < upper_bound_; i += step_) {
        result.push_back(i);
    }
    return result;
}
