#ifndef NORMAL_GENERATOR_H

#define NORMAL_GENERATOR_H

#include <random>

#include "Generator.h"

class NormalGenerator : public Generator
{
public:
    NormalGenerator(int mean, int stddev, int count);

    NormalGenerator(NormalGenerator&) = default;

    virtual std::vector<int> generate() const override;
    
    virtual ~NormalGenerator() = default;
private:
    int mean_;
    int stddev_;
    int count_;
    
    mutable std::mt19937 gen_;
};

#endif // !NORMAL_GENERATOR_H