#ifndef DERIVED_H

#define DERIVED_H

#include "base.hpp"

class Derived : public Base
{
public:
    virtual int solve() override { return 42; }
};

#endif // !DERIVED_H
