#ifndef DERIVED2_H

#define DERIVED2_H

#include "base.hpp"

class Derived2 : public Base
{
public:
    virtual int solve() override { return 0; }
};

#endif // !DERIVED2_HPP
