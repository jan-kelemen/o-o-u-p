#include <iostream>

#include "client.hpp"

void Client::operate()
{
    std::cout << solver_.solve() << "\n";
}