#ifndef DERIVED3_H

#define DERIVED3_H

#include "base.hpp"

class Derived3 : public Base
{
    virtual int solve() override { return 3; }
};
#endif // !DERIVED3_H
