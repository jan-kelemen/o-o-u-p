#ifndef CLIENT_H

#define CLIENT_H

#include "base.hpp"

class Client
{
public:
    Client(Base& b) : solver_(b) {}
    void operate();
private:
    Base& solver_;
};

#endif // !CLIENT_H
