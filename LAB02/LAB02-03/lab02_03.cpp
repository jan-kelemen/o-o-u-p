#include <cstdio>
#include <iostream>

class AbstractDatabase
{
public:
    virtual void get_data() = 0;
};

class ConcreteDatabase : public AbstractDatabase
{
public:
    virtual void get_data() override { std::cerr << "in ConcreteDatabase::get_data()\n"; };
};

class MockDatabase : public AbstractDatabase
{
public:
    virtual void get_data() override { std::cerr << "in MockDatabase::get_data()\n"; };
};

class Client1
{
    ConcreteDatabase my_database_;
public:
    Client1() : my_database_() {}
    void transaction() { std::cerr << "in Client1::transaction()\n"; my_database_.get_data(); }
};

class Client2
{
    AbstractDatabase& my_database_;
public:
    Client2(AbstractDatabase& db) : my_database_(db) {}
    void transaction() { std::cerr << "in Client2::transaction()\n"; my_database_.get_data(); }
};

int main()
{
    auto client1 = Client1();
    auto concrete_database = ConcreteDatabase();
    auto mock_database = MockDatabase();
    auto client2_1 = Client2(concrete_database);
    auto client2_2 = Client2(mock_database);

    client1.transaction();
    client2_1.transaction();
    client2_2.transaction();

    system("pause");
}