#ifndef TIGER_H

#define TIGER_H

#include "Types.h"

typedef struct
{
    PTRFUN* vtable;
    const char* name;
} Tiger;

void construct(Tiger* tiger, char const* name);

void* create(char const* name);

char const* name(void* tiger);
char const* greet(void* tiger);
char const* menu(void* tiger);

PTRFUN vtable[] = {name, greet, menu};

#endif // !TIGER_H