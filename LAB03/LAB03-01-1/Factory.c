#include <Windows.h>

#include "Factory.h"

typedef void* (*CRTFUNPTR)(char const*);

void* factory(char const* libname, char const* ctorarg)
{
    HMODULE hGetProcIDDLL = LoadLibrary(libname);

    if (!hGetProcIDDLL) {
        return NULL;
    }

    CRTFUNPTR create = (CRTFUNPTR) GetProcAddress(hGetProcIDDLL, "create");

    if (!create) {
        return NULL;
    }

    return create(ctorarg);
}
