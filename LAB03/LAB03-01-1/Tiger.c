#include <stdlib.h>

#include "Tiger.h"

void construct(Tiger* tiger, char const* name)
{
    tiger->vtable = vtable;
    tiger->name = name;
}

void* create(char const* name)
{
    void* ptr = malloc(sizeof(Tiger));
    construct((Tiger*) ptr, name);
    return ptr;
}

char const* name(void* tiger)
{
    return ((Tiger*)tiger)->name;
}

char const* greet(void* tiger)
{
    return "Mijau!";
}

char const* menu(void* tiger)
{
    return "mlako mlijeko";
}
