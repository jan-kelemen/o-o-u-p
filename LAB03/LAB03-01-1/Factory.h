#ifndef FACTORY_H

#define FACTORY_H

void *factory(char const* libname, char const* name);

#endif // !FACTORY_H