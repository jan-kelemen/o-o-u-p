#ifndef PARROT_H

#define PARROT_H

#include "Types.h"

typedef struct
{
    PTRFUN* vtable;
    const char* name;
} Parrot;

void construct(Parrot* parrot, char const* name);

void* create(char const* name);

char const* name(void* parrot);
char const* greet(void* parrot);
char const* menu(void* parrot);

PTRFUN vtable[] = {name, greet, menu};

#endif // !PARROT_H
