#include <stdio.h>
#include <stdlib.h>

#include "Animal.h"
#include "Factory.h"

void print_greeting(Animal* animal);
void print_menu(Animal* animal);

int main()
{
    Animal* p1 = (Animal*) factory("parrot", "Mudrobradi");
    Animal* p2 = (Animal*) factory("tiger", "Strasko");

    if (!p1 || !p2) {
        printf("Creation of plug-in objects failed.\n");
        exit(1);
    }

    print_greeting(p1);
    print_greeting(p2);

    print_menu(p1);
    print_menu(p2);

    free(p1);
    free(p2);

    system("pause");

    return 0;
}

void print_greeting(Animal* animal)
{
    printf("%s pozdravlja: %s\n", (animal->vtable[0])(animal), (animal->vtable[1])(animal));
}

void print_menu(Animal* animal)
{
    printf("%s voli: %s\n", (animal->vtable[0])(animal), (animal->vtable[2])(animal));
}
