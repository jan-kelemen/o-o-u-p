#include <stdlib.h>

#include "Parrot.h"

void construct(Parrot* parrot, const char* name)
{
    parrot->vtable = vtable;
    parrot->name = name;
}

void* create(char const* name)
{
    void* ptr = malloc(sizeof(Parrot));
    construct((Parrot*) ptr, name);
    return ptr;
}

char const* name(void* parrot)
{
    return ((Parrot*) parrot)->name;
}

char const* greet(void* parrot)
{
    return "Sto mu gromova!";
}

char const* menu(void* menu)
{
    return "brazilske orahe";
}
