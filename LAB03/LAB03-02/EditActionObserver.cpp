#include "EditActionObserver.h"

#include "EditActionSubject.h"

bool operator==(EditActionObserver const& lhs, EditActionObserver const& rhs)
{
    return std::addressof(lhs) == std::addressof(rhs);
}

bool operator!=(EditActionObserver const& lhs, EditActionObserver const& rhs) { return !(lhs == rhs); }