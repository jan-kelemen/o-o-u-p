#include "CursorObserver.h"

#include "CursorSubject.h"

bool operator==(CursorObserver const& lhs, CursorObserver const& rhs)
{
    return std::addressof(lhs) == std::addressof(rhs);
}

bool operator!=(CursorObserver const& lhs, CursorObserver const& rhs) { return !(lhs == rhs); }