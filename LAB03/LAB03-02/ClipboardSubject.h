#ifndef CLIPBOARD_SUBJECT_H

#define CLIPBOARD_SUBJECT_H

#include <vector>

#include "ClipboardObserver.h"

class ClipboardSubject
{
public:
    void attach(ClipboardObserver& ob);

    void detach(ClipboardObserver& ob);

protected:
    void notify(ClipboardSubject& subject) const;

private:
    std::vector<std::reference_wrapper<ClipboardObserver>> observers;
};
#endif // !CLIPBOARD_SUBJECT_H
