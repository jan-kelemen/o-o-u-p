#include "TextEditorModel.h"

#include <algorithm>
#include <functional>
#include <memory>
#include <sstream>

#include "InsertAction.h"
#include "DeleteAfterAction.h"
#include "DeleteBeforeAction.h"
#include "DeleteRangeAction.h"
#include "UndoManager.h"
#include "TextUtility.h"

auto split_string(const std::string &input, char delimiter)
{
    auto stream = std::istringstream{input};
    std::string substring;
    std::vector<std::string> tokens;
    while (std::getline(stream, substring, delimiter)) {
        tokens.emplace_back(substring);
    }
    return tokens;
}

TextEditorModel::TextEditorModel(std::string const& lines) 
    : lines_{split_string(lines, '\n')} 
    , cursor_location_{}
    , selection_range_{}
{}

std::pair<TextEditorModel::iterator, TextEditorModel::iterator> TextEditorModel::all_lines() 
{
    return lines_range(0, lines_.size());
}

std::pair<TextEditorModel::iterator, TextEditorModel::iterator> TextEditorModel::lines_range(int begin, int end)
{
    return std::make_pair(iterator(lines_, begin, end), iterator(lines_, end, end));
}

std::vector<std::string>& TextEditorModel::lines() { return lines_; }

LocationRange& TextEditorModel::selection_range() { return selection_range_; }

Location& TextEditorModel::cursor_location() { return cursor_location_; }

void TextEditorModel::notify_observers(bool text_observers, bool cursor_observers)
{
    if (text_observers) {
        TextSubject::notify(*this);
    }
    if (cursor_observers) {
        CursorSubject::notify(cursor_location_);
    }
}

void TextEditorModel::move_cursor_left()
{
    clear_selection_range();
    auto l = cursor_left(lines_, cursor_location_);
    if (cursor_location_ != l) {
        cursor_location_ = l;
        CursorSubject::notify(l);
    }
}

void TextEditorModel::move_cursor_right()
{
    clear_selection_range();
    auto l = cursor_right(lines_, cursor_location_);
    if (cursor_location_ != l) {
        cursor_location_ = l;
        CursorSubject::notify(l);
    }
}

void TextEditorModel::move_cursor_up()
{
    clear_selection_range();
    auto l = cursor_up(lines_, cursor_location_);
    if (cursor_location_ != l) {
        cursor_location_ = l;
        CursorSubject::notify(l);
    }
}

void TextEditorModel::move_cursor_down()
{
    clear_selection_range();
    auto l = cursor_down(lines_, cursor_location_);
    if (cursor_location_ != l) {
        cursor_location_ = l;
        CursorSubject::notify(l);
    }
}

void TextEditorModel::move_selection_range_left()
{
    if (on_document_beginning(lines_, cursor_location_)) { return; }

    if (!selection_range_.exists()) {
        selection_range_.assign(cursor_location_, cursor_location_);
    }

    auto original = selection_range_;
    move_cursor_left();
    selection_range_ = original;

    if (cursor_location_ < selection_range_.first()) {
        selection_range_.first() = cursor_location_;
    }
    else {
        selection_range_.second() = cursor_location_;
    }

    CursorSubject::notify(cursor_location_);
}

void TextEditorModel::move_selection_range_right()
{
    if (on_document_end(lines_, cursor_location_)) { return; }

    if (!selection_range_.exists()) {
        selection_range_.assign(cursor_location_, cursor_location_);
    }

    auto original = selection_range_;
    move_cursor_right();
    selection_range_ = original;

    if (cursor_location_ > selection_range_.second()) {
        selection_range_.second() = cursor_location_;
    }
    else {
        selection_range_.first() = cursor_location_;
    }

    CursorSubject::notify(cursor_location_);
}

void TextEditorModel::move_selection_range_up()
{
    if (on_document_beginning(lines_, cursor_location_)) { return; }

    if (!selection_range_.exists()) {
        selection_range_.assign(cursor_location_, cursor_location_);
    }

    auto original = selection_range_;
    move_cursor_up();
    selection_range_ = original;

    if (cursor_location_ < selection_range_.first()) {
        selection_range_.first() = cursor_location_;
    }
    else {
        selection_range_.second() = cursor_location_;
    }

    CursorSubject::notify(cursor_location_);
}

void TextEditorModel::move_selection_range_down()
{
    if (on_document_end(lines_, cursor_location_)) { return; }

    if (!selection_range_.exists()) {
        selection_range_.assign(cursor_location_, cursor_location_);
    }

    auto original = selection_range_;
    move_cursor_down();
    selection_range_ = original;

    if (cursor_location_ > selection_range_.second()) {
        selection_range_.second() = cursor_location_;
    }
    else {
        selection_range_.first() = cursor_location_;
    }

    CursorSubject::notify(cursor_location_);
}

std::string TextEditorModel::selection_range_text() const
{
    if (!selection_range_.exists()) { return ""; }

    if (selection_range_.first().row() == selection_range_.second().row()) {
        return lines_[selection_range_.first().row()]
            .substr(selection_range_.first().column(), 
                    selection_range_.second().column() - selection_range_.first().column());
    }

    auto first = lines_[selection_range_.first().row()].substr(selection_range_.first().column());

    for (auto i = selection_range_.first().row() + 1; i < selection_range_.second().row(); i++) {
        first += '\n' + lines_[i];
    }

    auto last = '\n' + lines_[selection_range_.second().row()].substr(0, selection_range_.second().column());

    return first + last;
}

void TextEditorModel::delete_before()
{
    if (!DeleteBeforeAction::executable(*this)) { return; }

    auto action = std::make_shared<DeleteBeforeAction>(*this);
    action->execute();
    UndoManager::instance().push(action);
}

void TextEditorModel::delete_after()
{
    if (!DeleteAfterAction::executable(*this)) { return; }

    auto action = std::make_shared<DeleteAfterAction>(*this);
    action->execute();
    UndoManager::instance().push(action);
}

void TextEditorModel::delete_range(LocationRange const& range)
{
    if (!DeleteRangeAction::executable(*this)) { return; }

    auto action = std::make_shared<DeleteRangeAction>(*this, range);
    action->execute();
    UndoManager::instance().push(action);
}

void TextEditorModel::insert(char c) { insert(std::string(1, c)); }

void TextEditorModel::insert(std::string const& text)
{
    if (!InsertAction::executable(*this)) { return; }

    auto action = std::make_shared<InsertAction>(*this, text);
    action->execute();
    UndoManager::instance().push(action);
}

void TextEditorModel::clear_selection_range() { selection_range_ = LocationRange(); }

TextEditorModel::TextEditorModelIterator::TextEditorModelIterator(std::vector<std::string>& lines)
    : TextEditorModelIterator(lines, 0, lines.size()) 
{}

TextEditorModel::TextEditorModelIterator::TextEditorModelIterator(std::vector<std::string>& lines, int begin, int end)
    : lines_{lines}
    , current_{begin}
    , end_{end}
{}

TextEditorModel::iterator& TextEditorModel::TextEditorModelIterator::operator++() { ++current_; return *this; }

TextEditorModel::iterator TextEditorModel::TextEditorModelIterator::operator++(int) 
{ 
    auto rv = *this; 
    operator++(); 
    return rv; 
}

bool TextEditorModel::TextEditorModelIterator::operator==(const TextEditorModelIterator& rhs)
{
    return current_ == rhs.current_;
}

bool TextEditorModel::TextEditorModelIterator::operator!=(const TextEditorModelIterator& rhs) { return !(*this == rhs); }

std::string& TextEditorModel::TextEditorModelIterator::operator*() { return lines_[current_]; }

