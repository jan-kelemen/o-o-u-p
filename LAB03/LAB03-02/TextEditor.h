#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <memory>

#include <QWidget>
#include <QKeyEvent>
#include "ui_TextEditor.h"

#include "ClipboardStack.h"
#include "CursorObserver.h"
#include "TextEditorModel.h"

class TextEditor 
    : public QWidget
    , public CursorObserver
    , public TextObserver
{
    Q_OBJECT

public:
    TextEditor(QWidget *parent);
    ~TextEditor() = default;

    virtual void update(Location const& state) override;

    virtual void update(TextSubject& subject) override;

    void paintEvent(QPaintEvent*);

    void keyPressEvent(QKeyEvent* e);
private:
    Ui::TextEditor ui;
    std::unique_ptr<TextEditorModel> model;
    std::unique_ptr<ClipboardStack> clipboard;
};

#endif // TEXTEDITOR_H
