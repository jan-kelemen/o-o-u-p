#ifndef UNDO_MANAGER_H

#define UNDO_MANAGER_H

#include <memory>
#include <stack>

#include "EditAction.h"
#include "EditActionSubject.h"

class UndoManager : public EditActionSubject
{
public:
    static UndoManager& instance();
    UndoManager(UndoManager const&) = delete;
    void operator=(UndoManager const&) = delete;

    void undo();

    void redo();

    void push(std::shared_ptr<EditAction> action);

    virtual bool redo_available() const override;

    virtual bool undo_available() const override;

private:
    UndoManager() = default;

    std::stack<std::shared_ptr<EditAction>> undo_stack;
    std::stack<std::shared_ptr<EditAction>> redo_stack;
};
#endif // !UNDO_MANAGER_H
