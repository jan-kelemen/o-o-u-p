#include "CursorSubject.h"

#include <algorithm>

void CursorSubject::attach(CursorObserver& ob)
{ 
    observers.push_back(ob); 
}

void CursorSubject::detach(CursorObserver& ob)
{
    observers.erase(std::remove(observers.begin(), observers.end(), std::reference_wrapper<CursorObserver>(ob)));
}

void CursorSubject::notify(Location const& location) const
{
    std::for_each(observers.begin(), observers.end(), [&location](CursorObserver& o) { o.update(location); });
}
