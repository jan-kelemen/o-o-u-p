#ifndef CLIPBOARD_STACK_H

#define CLIPBOARD_STACK_H

#include <stack>
#include <string>

#include "ClipboardSubject.h"

class ClipboardStack : public ClipboardSubject
{
public:
    void push(std::string const& text);

    std::string pop();

    std::string top();
private:
    std::stack<std::string> texts;
};
#endif // !CLIPBOARD_STACK_H
