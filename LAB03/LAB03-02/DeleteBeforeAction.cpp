#include "DeleteBeforeAction.h"

#include "DeleteRangeAction.h"
#include "TextEditorModel.h"
#include "TextUtility.h"

bool DeleteBeforeAction::executable(TextEditorModel& model)
{
    return DeleteRangeAction::executable(model) || !on_document_beginning(model.lines(), model.cursor_location());
}

DeleteBeforeAction::DeleteBeforeAction(TextEditorModel& model)
    : EditAction{model}
    , location_{model.cursor_location()}
    , revert_location_{cursor_left(model.lines(), model.cursor_location())}
    , delete_range_{DeleteRangeAction::executable(model) ? std::make_unique<DeleteRangeAction>(model) : nullptr}
{}

void DeleteBeforeAction::execute()
{
    if (delete_range_ != nullptr) { delete_range_->execute(); return; }

    auto& lines = model().lines();
    auto row = location_.row();
    auto col = location_.column();

    auto new_cursor = cursor_left(lines, location_);

    if (on_line_beginning(lines, location_)) {
        c_ = '\n';
        lines[row - 1] += lines[row];
        lines.erase(lines.begin() + row);
    }
    else {
        c_ = lines[location_.row()][location_.column() - 1];
        lines[row].erase(lines[row].begin() + col - 1);
    }

    model().cursor_location() = new_cursor;
    model().selection_range() = LocationRange();

    model().notify_observers();
}

void DeleteBeforeAction::revert()
{
    if (delete_range_ != nullptr) { delete_range_->revert(); return; }

    model().cursor_location() = insert(model().lines(), revert_location_, c_);
    model().selection_range() = LocationRange();

    model().notify_observers();
}
