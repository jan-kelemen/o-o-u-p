#ifndef LAB0302_H
#define LAB0302_H

#include <QtWidgets/QMainWindow>
#include "ui_lab0302.h"
#include "TextEditor.h"

class LAB0302 : public QMainWindow
{
    Q_OBJECT

public:
    LAB0302(QWidget *parent = 0);
    ~LAB0302() = default;

private:
    void keyPressEvent(QKeyEvent* e);

    Ui::LAB0302Class ui;
    TextEditor* editor;
};

#endif // LAB0302_H
