#include "ClipboardObserver.h"

#include <memory>

bool operator==(ClipboardObserver const& lhs, ClipboardObserver const& rhs)
{
    return std::addressof(lhs) == std::addressof(rhs);
}

bool operator!=(ClipboardObserver const& lhs, ClipboardObserver const& rhs) { return !(lhs == rhs); }