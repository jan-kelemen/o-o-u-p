#ifndef EDIT_ACTION_SUBJECT_H

#define EDIT_ACTION_SUBJECT_H

#include <vector>

#include "EditActionObserver.h"

class EditActionSubject
{
public:
    void attach(EditActionObserver& ob);

    void detach(EditActionObserver& ob);

    virtual bool redo_available() const = 0;

    virtual bool undo_available() const = 0;

protected:
    void notify(EditActionSubject& subject) const;

private:
    std::vector<std::reference_wrapper<EditActionObserver>> observers;
};

#endif // !EDIT_ACTION_SUBJECT_H
