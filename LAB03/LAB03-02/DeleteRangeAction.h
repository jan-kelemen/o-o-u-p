#ifndef DELETE_RANGE_ACTION_H

#define DELETE_RANGE_ACTION_H

#include <string>

#include "EditAction.h"
#include "LocationRange.h"

class DeleteRangeAction : public EditAction
{
public:
    static bool executable(TextEditorModel& model);

    DeleteRangeAction(TextEditorModel& model);

    DeleteRangeAction(TextEditorModel& model, LocationRange const& range);

    virtual void execute() override;

    virtual void revert() override;
private:
    std::string text_;
    LocationRange range_;
};
#endif // !DELETE_RANGE_ACTION_H
