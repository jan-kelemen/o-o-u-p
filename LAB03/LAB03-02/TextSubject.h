#ifndef TEXT_SUBJECT_H

#define TEXT_SUBJECT_H

#include <vector>

#include "TextObserver.h"

class TextSubject
{
public:
    void attach(TextObserver& ob);

    void detach(TextObserver& ob);

protected:
    void notify(TextSubject& subject) const;

private:
    std::vector<std::reference_wrapper<TextObserver>> observers;
};
#endif // !TEXT_SUBJECT_H
