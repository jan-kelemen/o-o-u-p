#include "EditAction.h"

EditAction::EditAction(TextEditorModel& model) : model_{model} {}

TextEditorModel & EditAction::model() const { return model_; }