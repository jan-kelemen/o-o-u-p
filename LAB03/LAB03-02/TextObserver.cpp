#include "TextObserver.h"

#include "TextSubject.h"

bool operator==(TextObserver const& lhs, TextObserver const& rhs)
{
    return std::addressof(lhs) == std::addressof(rhs);
}

bool operator!=(TextObserver const& lhs, TextObserver const& rhs) { return !(lhs == rhs); }