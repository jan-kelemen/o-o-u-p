#include "ClipboardSubject.h"

#include <algorithm>

void ClipboardSubject::attach(ClipboardObserver& ob)
{
    observers.push_back(ob);
}

void ClipboardSubject::detach(ClipboardObserver& ob)
{
    observers.erase(std::remove(observers.begin(), observers.end(), std::reference_wrapper<ClipboardObserver>(ob)));
}

void ClipboardSubject::notify(ClipboardSubject& subject) const
{
    std::for_each(observers.begin(), observers.end(), [&subject](ClipboardObserver& o) { o.update(subject); });
}
