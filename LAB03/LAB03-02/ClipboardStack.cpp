#include "ClipboardStack.h"

void ClipboardStack::push(std::string const& text) { texts.push(text); notify(*this); }

std::string ClipboardStack::pop() { auto rv = texts.top(); texts.pop(); notify(*this);  return rv; }

std::string ClipboardStack::top() { return texts.top(); }