#ifndef EDIT_ACTION_H

#define EDIT_ACTION_H

class TextEditorModel;

class EditAction
{
public:
    EditAction(TextEditorModel& model);

    virtual void execute() = 0;

    virtual void revert() = 0;

    TextEditorModel& model() const;
private:
    TextEditorModel& model_;
};

#endif // !EDIT_ACTION_H
