#ifndef TEXT_OBSERVER_H

#define TEXT_OBSERVER_H

class TextSubject;

#include "Location.h"

class TextObserver
{
public:
    virtual void update(TextSubject& subject) = 0;

    virtual ~TextObserver() = default;
};

bool operator==(TextObserver const& lhs, TextObserver const& rhs);
bool operator!=(TextObserver const& lhs, TextObserver const& rhs);

#endif // !TEXT_OBSERVER_H
