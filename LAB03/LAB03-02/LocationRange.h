#ifndef LOCATION_RANGE_H

#define LOCATION_RANGE_H

#include "Location.h"

class LocationRange
{
public:
    LocationRange() = default;

    LocationRange(Location const& first, Location const& second);

    void assign(Location const& first, Location const& second);

    bool exists() const;

    bool in_range(Location const& location) const;

    Location& first();
    Location const& first() const;

    Location& second();
    Location const& second() const;
private:
    Location first_;
    Location second_;
};

#endif // !LOCATION_RANGE_H