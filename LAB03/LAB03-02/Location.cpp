#include "Location.h"

Location::Location() : Location(0, 0) {}

Location::Location(int row, int column) : row_{row}, column_{column} {}

int& Location::row() { return row_; }

int const& Location::row() const { return row_; }

int& Location::column() { return column_; }

int const& Location::column() const { return column_; }

bool operator==(Location const& lhs, Location const& rhs)
{
    return lhs.row() == rhs.row() && lhs.column() == rhs.column();
}

bool operator!=(Location const& lhs, Location const& rhs) { return !(lhs == rhs); }

bool operator<(Location const& lhs, Location const& rhs)
{
    if (lhs.row() < rhs.row()) { return true; }
    else if (lhs.row() == rhs.row()) { return lhs.column() < rhs.column(); }
    else { return false; }
}

bool operator<=(Location const& lhs, Location const& rhs) { return !(rhs < lhs); }

bool operator>=(Location const& lhs, Location const& rhs) { return !(lhs < rhs); }

bool operator>(Location const& lhs, Location const& rhs) { return rhs < lhs; }