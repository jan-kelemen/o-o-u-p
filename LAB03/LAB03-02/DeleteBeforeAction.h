#ifndef DELETE_BEFORE_ACTION_H

#define DELETE_BEFORE_ACTION_H

#include <memory>

#include "EditAction.h"
#include "Location.h"

class DeleteBeforeAction : public EditAction
{
public:
    static bool executable(TextEditorModel& model);

    DeleteBeforeAction(TextEditorModel& model);

    virtual void execute() override;

    virtual void revert() override;
private:
    std::unique_ptr<EditAction> delete_range_;

    char c_;
    Location location_;
    Location revert_location_;
};

#endif // !DELETE_BEFORE_ACTION_H
