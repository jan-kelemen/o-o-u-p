#ifndef TEXT_UTILITY_H

#define TEXT_UTILITY_H

#include <string>
#include <vector>

#include "Location.h"

Location cursor_left(std::vector<std::string> const& lines, Location const& cursor);

Location cursor_right(std::vector<std::string> const& lines, Location const& cursor);

Location cursor_up(std::vector<std::string> const& lines, Location const& cursor);

Location cursor_down(std::vector<std::string> const& lines, Location const& cursor);

Location insert(std::vector<std::string>& lines, Location const& location, char c);

Location insert(std::vector<std::string>& lines, Location const& location, std::string const& text);

bool on_document_beginning(std::vector<std::string> const& lines, Location const& cursor);

bool on_document_end(std::vector<std::string> const& lines, Location const& cursor);

bool on_line_beginning(std::vector<std::string> const& lines, Location const& cursor);

bool on_line_end(std::vector<std::string> const& lines, Location const& cursor);

bool on_first_line(std::vector<std::string> const& lines, Location const& cursor);

bool on_last_line(std::vector<std::string> const& lines, Location const& cursor);

#endif // !TEXT_UTILITY_H
