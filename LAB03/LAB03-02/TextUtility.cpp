#include "TextUtility.h"

#include <algorithm>

Location cursor_left(std::vector<std::string> const& lines, Location const& cursor)
{
    auto row = cursor.row();
    auto col = cursor.column();

    if (!on_document_beginning(lines, cursor)) {
        if (!on_line_beginning(lines, cursor)) { --col; }
        else if (!on_first_line(lines, cursor)) { col = lines[--row].length(); }
    }

    return {row, col};
}

Location cursor_right(std::vector<std::string> const& lines, Location const& cursor)
{
    auto row = cursor.row();
    auto col = cursor.column();

    if (!on_document_end(lines, cursor)) {
        if (!on_line_end(lines, cursor)) { ++col; }
        else if (!on_last_line(lines, cursor)) { ++row; col = 0; }
    }

    return {row, col};
}

Location cursor_up(std::vector<std::string> const& lines, Location const& cursor)
{
    auto row = cursor.row();
    auto col = cursor.column();

    if (!on_first_line(lines, cursor)) {
        --row;
        col = std::min(static_cast<int>(lines[row].length()), col);
    }
    else {
        //go to beggining of the document
        col = 0;
    }

    return {row, col};
}

Location cursor_down(std::vector<std::string> const& lines, Location const& cursor)
{
    auto row = cursor.row();
    auto col = cursor.column();

    if (!on_last_line(lines, cursor)) {
        ++row;
        col = std::min(static_cast<int>(lines[row].length()), col);
    }
    else {
        //go to end of the document
        col = lines[row].length();
    }

    return {row, col};
}

Location insert(std::vector<std::string>& lines, Location const& location, char c)
{
    auto& current = lines[location.row()];

    if (c == '\n') {
        auto parts = {current.substr(0, location.column()), current.substr(location.column())};
        lines.erase(lines.begin() + location.row());
        lines.insert(lines.begin() + location.row(), parts.begin(), parts.end());

        return{location.row() + 1, 0};
    }

    if (current.length() == location.column()) {
        current += c;
    }
    else {
        current.insert(location.column(), 1, c);
    }

    return{location.row(), location.column() + 1};
}

Location insert(std::vector<std::string>& lines, Location const& location, std::string const& text)
{
    auto loc = location;
    for (auto c : text) {
        loc = insert(lines, loc, c);
    }
    return loc;
}

bool on_document_beginning(std::vector<std::string> const& lines, Location const& cursor) 
{ 
    return on_first_line(lines, cursor) && on_line_beginning(lines, cursor); 
}

bool on_document_end(std::vector<std::string> const& lines, Location const& cursor) 
{ 
    return on_last_line(lines, cursor) && on_line_end(lines, cursor); 
}

bool on_line_beginning(std::vector<std::string> const& lines, Location const& cursor) 
{ 
    return cursor.column() == 0; 
}

bool on_line_end(std::vector<std::string> const& lines, Location const& cursor)
{
    return cursor.column() == lines[cursor.row()].length();
}

bool on_first_line(std::vector<std::string> const& lines, Location const& cursor) 
{ 
    return cursor.row() == 0; 
}

bool on_last_line(std::vector<std::string> const& lines, Location const& cursor)
{ 
    return cursor.row() == lines.size() - 1;
}
