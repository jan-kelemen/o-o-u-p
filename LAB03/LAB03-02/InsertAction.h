#ifndef INSERT_TEXT_ACTION_H

#define INSERT_TEXT_ACTION_H

#include <memory>
#include <string>

#include "EditAction.h"

class InsertAction : public EditAction
{
public:
    static bool executable(TextEditorModel& model);

    InsertAction(TextEditorModel& model, std::string const& text);

    virtual void execute() override;

    virtual void revert() override;
private:
    std::unique_ptr<EditAction> delete_range_;

    std::unique_ptr<EditAction> revert_action_;
    std::string text_;
};

#endif // !INSERT_TEXT_ACTION_H
