#include "DeleteRangeAction.h"

#include "TextEditorModel.h"

#include "TextUtility.h"

bool DeleteRangeAction::executable(TextEditorModel& model) { return model.selection_range().exists(); }

DeleteRangeAction::DeleteRangeAction(TextEditorModel& model) : DeleteRangeAction(model, model.selection_range()) {}

DeleteRangeAction::DeleteRangeAction(TextEditorModel& model, LocationRange const& range)
    : EditAction{model}
    , range_{range}
    , text_{model.selection_range_text()} 
{}

void DeleteRangeAction::execute()
{
    auto& lines = model().lines();
    auto first_row = range_.first().row();
    auto first_col = range_.first().column();
    auto second_row = range_.second().row();
    auto second_col = range_.second().column();

    auto partial_first = lines[first_row].substr(0, first_col);
    auto partial_last = lines[second_row].substr(second_col);

    auto begin = lines.begin() + first_row;
    lines.erase(begin, lines.begin() + second_row);
    lines[first_row] = partial_first + partial_last;

    model().cursor_location() = range_.first();
    model().selection_range() = LocationRange();

    model().notify_observers();
}

void DeleteRangeAction::revert()
{
    model().cursor_location() = insert(model().lines(), range_.first(), text_);
    model().selection_range() = LocationRange();

    model().notify_observers();
}
