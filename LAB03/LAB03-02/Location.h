#ifndef LOCATION_H

#define LOCATION_H

#include <cstdint>

class Location
{
public:
    Location();

    Location(int row, int column);

    int& row();
    int const& row() const;

    int& column();
    int const& column() const;

private:
    int row_;
    int column_;
};

bool operator==(Location const& lhs, Location const& rhs);
bool operator!=(Location const& lhs, Location const& rhs);

bool operator<(Location const& lhs, Location const& rhs);
bool operator<=(Location const& lhs, Location const& rhs);
bool operator>=(Location const& lhs, Location const& rhs);
bool operator>(Location const& lhs, Location const& rhs);

#endif // !LOCATION_H
