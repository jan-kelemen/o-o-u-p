#include "EditActionSubject.h"

#include <algorithm>

void EditActionSubject::attach(EditActionObserver& ob)
{
    observers.push_back(ob);
}

void EditActionSubject::detach(EditActionObserver& ob)
{
    observers.erase(std::remove(observers.begin(), observers.end(), std::reference_wrapper<EditActionObserver>(ob)));
}

void EditActionSubject::notify(EditActionSubject& subject) const
{
    std::for_each(observers.begin(), observers.end(), [&subject](EditActionObserver& o) { o.update(subject); });
}
