#ifndef CURSOR_SUBJECT_H

#define CURSOR_SUBJECT_H

#include <vector>

#include "CursorObserver.h"
#include "Location.h"

class CursorSubject
{
public:
    void attach(CursorObserver& ob);

    void detach(CursorObserver& ob);

protected:
    void notify(Location const& location) const;

private:
    std::vector<std::reference_wrapper<CursorObserver>> observers;
};
#endif // !CURSOR_SUBJECT_H
