#include "LocationRange.h"

LocationRange::LocationRange(Location const& first, Location const& second) : first_{first}, second_{second} {}

void LocationRange::assign(Location const& first, Location const& second)
{
    if (first < second) { first_ = first; second_ = second; }
    else { second_ = first; first_ = second; }
}

bool LocationRange::exists() const { return first_ != second_; }

bool LocationRange::in_range(Location const& location) const 
{
    if (!exists()) { return false; }
    return first_ <= location && location < second_; 
}

Location& LocationRange::first() { return first_; }

Location const& LocationRange::first() const { return first_; }

Location& LocationRange::second() { return second_; }

Location const& LocationRange::second() const { return second_; }