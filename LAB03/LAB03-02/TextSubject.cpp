#include "TextSubject.h"

#include <algorithm>

void TextSubject::attach(TextObserver& ob)
{
    observers.push_back(ob);
}

void TextSubject::detach(TextObserver& ob)
{
    observers.erase(std::remove(observers.begin(), observers.end(), std::reference_wrapper<TextObserver>(ob)));
}

void TextSubject::notify(TextSubject& subject) const
{
    std::for_each(observers.begin(), observers.end(), [&subject](TextObserver& o) { o.update(subject); });
}
