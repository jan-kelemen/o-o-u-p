#ifndef TEXT_EDITOR_MODEL_H

#define TEXT_EDITOR_MODEL_H

#include <iterator>
#include <string>
#include <vector>

#include "LocationRange.h"
#include "CursorSubject.h"
#include "TextSubject.h"

class TextEditorModel
    : public CursorSubject
    , public TextSubject
{
    class TextEditorModelIterator 
        : public std::iterator<std::output_iterator_tag, std::string>
    {
    public:
        TextEditorModelIterator(std::vector<std::string>& lines);
        TextEditorModelIterator(std::vector<std::string>& lines, int begin, int end);
        TextEditorModelIterator(TextEditorModelIterator const&) = default;

        TextEditorModelIterator& operator++();
        TextEditorModelIterator operator++(int);

        bool operator==(const TextEditorModelIterator& rhs);
        bool operator!=(const TextEditorModelIterator& rhs);

        std::string& operator*();
    private:
        std::vector<std::string>& lines_;
        int current_;
        int end_;
    };

public:
    using iterator = TextEditorModelIterator;

    TextEditorModel(std::string const& lines);

    std::pair<iterator, iterator> all_lines();

    std::pair<iterator, iterator> lines_range(int begin, int end);

    std::vector<std::string>& lines();

    LocationRange& selection_range();

    Location& cursor_location();

    void notify_observers(bool text_observers = true, bool cursor_observers = true);

    // - - - Cursor methods
    void move_cursor_left();

    void move_cursor_right();
    
    void move_cursor_up();

    void move_cursor_down();

    // - - - Selection range methods
    void move_selection_range_left();

    void move_selection_range_right();

    void move_selection_range_up();

    void move_selection_range_down();

    std::string selection_range_text() const;

    // - - - Text methods
    void delete_before();

    void delete_after();

    void delete_range(LocationRange const& range);

    void insert(char c);

    void insert(std::string const& text);

private:
    void clear_selection_range();

    std::vector<std::string> lines_;
    LocationRange selection_range_;
    Location cursor_location_;
};

#endif // !TEXT_EDITOR_MODEL_H
