#include "TextEditor.h"

#include <QPainter>
#include <QString>

#include "UndoManager.h"

TextEditor::TextEditor(QWidget *parent)
    : QWidget(parent)
    , model{std::make_unique<TextEditorModel>("All\nHail\nTyrannizer\ntest\ntest")}
    , clipboard{std::make_unique<ClipboardStack>()}
{
    model->CursorSubject::attach(*this);
    model->TextSubject::attach(*this);

    ui.setupUi(this);
}

void TextEditor::update(Location const& state)
{
    repaint();
}

void TextEditor::update(TextSubject& subject)
{
    repaint();
}

void TextEditor::paintEvent(QPaintEvent *)
{
    using iter = TextEditorModel::iterator;

    QPainter painter(this);
    painter.setPen(Qt::black);
    painter.setFont(QFont("Consolas", 12));
    auto vertical_offset = 15;

    auto lines = model->all_lines();
    auto line = 0;
    for (auto cur = lines.first; cur != lines.second; ++cur, ++line) {
        for (auto i = 0; i < (*cur).length(); ++i) {
            painter.setPen(Qt::black);
            if (model->selection_range().in_range({line, i})) {
                painter.fillRect(i * 9, vertical_offset - 15, 9, 15, Qt::BrushStyle::SolidPattern);
                painter.setPen(Qt::white);
            }
            painter.drawText(i * 9, vertical_offset, QString((*cur)[i]));
        }
        vertical_offset += 15;
    }

    auto row = model->cursor_location().row();
    auto col = model->cursor_location().column();

    painter.drawLine(col * 9, row * 15, col * 9, (row + 1) * 15);
}

void TextEditor::keyPressEvent(QKeyEvent* e)
{
    bool hasShift = QApplication::keyboardModifiers().testFlag(Qt::ShiftModifier) || e->key() == Qt::Key_Shift;
    bool hasCtrl = QApplication::keyboardModifiers().testFlag(Qt::ControlModifier) || e->key() == Qt::Key_Control;
    bool hasAlt = QApplication::keyboardModifiers().testFlag(Qt::AltModifier) || e->key() == Qt::Key_Alt || Qt::Key_AltGr;

    auto key = e->key();
    switch (key) {
        case Qt::Key_Up:
            if (hasShift) { model->move_selection_range_up(); }
            else { model->move_cursor_up(); }
            break;
        case Qt::Key_Down:
            if (hasShift) { model->move_selection_range_down(); }
            else { model->move_cursor_down(); }
            break;
        case Qt::Key_Left:
            if (hasShift) { model->move_selection_range_left(); }
            else { model->move_cursor_left(); }
            break;
        case Qt::Key_Right:
            if (hasShift) { model->move_selection_range_right(); }
            else { model->move_cursor_right(); }
            break;
        case Qt::Key_Backspace:
            model->delete_before(); break;
        case Qt::Key_Delete:
            model->delete_after(); break;
        case Qt::Key_Enter:
        case Qt::Key_Return:
            model->insert('\n'); break;
        case Qt::Key_C:
            if (hasCtrl) {
                //CTRL + C - copy
                if (model->selection_range().exists()) { 
                    clipboard->push(model->selection_range_text()); 
                }
            }
            else { model->insert(key); }
            break;
        case Qt::Key_X:
            if (hasCtrl) {
                //CTRL + X - cut
                if (model->selection_range().exists()) {
                    clipboard->push(model->selection_range_text());
                    model->delete_range(model->selection_range());
                }
            }
            else { model->insert(key); }
            break;
        case Qt::Key_V:
            if (hasCtrl) {
                //CTRL + SHIFT + V - paste and pop
                if (hasShift) {
                    model->insert(clipboard->pop());
                }
                //CTRL + V - paste
                else {
                    model->insert(clipboard->top());
                }
            }
            else { model->insert(key); }
            break;
        case Qt::Key_Z:
            if (hasCtrl) {
                //CTRL + Z - undo
                if (UndoManager::instance().undo_available()) {
                    UndoManager::instance().undo();
                }
            }
            else { model->insert(key); }
            break;
        case Qt::Key_Y:
            if (hasCtrl) {
                //CTRL + Y - redo
                if (UndoManager::instance().redo_available()) {
                    UndoManager::instance().redo();
                }
            }
            else { model->insert(key); }
            break;
        case Qt::Key_Shift:
        case Qt::Key_Control:
        case Qt::Key_Alt:
        case Qt::Key_AltGr:
            //modifier key presses are ignored
            break;
        default:
            model->insert(e->text().toStdString()[0]);
            break;
    }
}
