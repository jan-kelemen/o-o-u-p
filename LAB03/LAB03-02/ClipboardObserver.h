#ifndef CLIPBOARD_OBSERVER_H

#define CLIPBOARD_OBSERVER_H

class ClipboardSubject;

class ClipboardObserver
{
public:
    virtual void update(ClipboardSubject& subject) = 0;

    virtual ~ClipboardObserver() = default;
};

bool operator==(ClipboardObserver const& lhs, ClipboardObserver const& rhs);
bool operator!=(ClipboardObserver const& lhs, ClipboardObserver const& rhs);

#endif // !CLIPBOARD_OBSERVER_H
