#include "UndoManager.h"

UndoManager& UndoManager::instance()
{
    static UndoManager m;
    return m;
}

void UndoManager::undo()
{
    auto a = undo_stack.top(); undo_stack.pop();
    redo_stack.push(a);
    a->revert();

    notify(*this);
}

void UndoManager::redo()
{
    auto a = redo_stack.top(); redo_stack.pop();
    a->execute();
    undo_stack.push(a);

    notify(*this);
}

void UndoManager::push(std::shared_ptr<EditAction> a)
{
    redo_stack = std::stack<std::shared_ptr<EditAction>>();
    undo_stack.push(a);

    notify(*this);
}

bool UndoManager::redo_available() const { return !redo_stack.empty(); }

bool UndoManager::undo_available() const { return !undo_stack.empty(); }
