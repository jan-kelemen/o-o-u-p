#include "DeleteAfterAction.h"

#include "DeleteRangeAction.h"
#include "TextEditorModel.h"
#include "TextUtility.h"

bool DeleteAfterAction::executable(TextEditorModel& model)
{
    return DeleteRangeAction::executable(model) || !on_document_end(model.lines(), model.cursor_location());
}

DeleteAfterAction::DeleteAfterAction(TextEditorModel& model)
    : EditAction{model}
    , location_{model.cursor_location()}
    , delete_range_{DeleteRangeAction::executable(model) ? std::make_unique<DeleteRangeAction>(model) : nullptr}
{}

void DeleteAfterAction::execute()
{
    if (delete_range_ != nullptr) { delete_range_->execute(); return; }

    auto& lines = model().lines();
    auto row = location_.row();
    auto col = location_.column();

    if (on_line_end(lines, location_)) {
        c_ = '\n';
        lines[row] += lines[row + 1];
        lines.erase(lines.begin() + row + 1);
    }
    else {
        c_ = lines[location_.row()][location_.column()];
        lines[row].erase(lines[row].begin() + col);
    }

    model().selection_range() = LocationRange();
    model().notify_observers(true, false);
}

void DeleteAfterAction::revert()
{
    if (delete_range_ != nullptr) { delete_range_->revert(); return; }

    model().cursor_location() = insert(model().lines(), location_, c_);
    model().selection_range() = LocationRange();

    model().notify_observers();
}
