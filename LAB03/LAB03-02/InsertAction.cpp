#include "InsertAction.h"

#include "DeleteRangeAction.h"
#include "TextEditorModel.h"
#include "TextUtility.h"

bool InsertAction::executable(TextEditorModel& model) { return true; }

InsertAction::InsertAction(TextEditorModel& model, std::string const& text)
    : EditAction{model}
    , text_{text}
    , delete_range_{DeleteRangeAction::executable(model) ? std::make_unique<DeleteRangeAction>(model) : nullptr}
    , revert_action_{nullptr}
{}

void InsertAction::execute()
{
    if (delete_range_ != nullptr) { delete_range_->execute(); }
    auto end = insert(model().lines(), model().cursor_location(), text_);

    if(revert_action_ == nullptr) { 
        revert_action_ = std::make_unique<DeleteRangeAction>(model(), LocationRange{model().cursor_location(), end}); 
    }

    model().selection_range() = LocationRange();
    model().cursor_location() = end;

    model().notify_observers();
}

void InsertAction::revert()
{
    revert_action_->execute();
    if (delete_range_ != nullptr) { delete_range_->revert(); }

    model().notify_observers();
}
