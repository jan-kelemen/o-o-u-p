#ifndef CURSOR_OBSERVER_H

#define CURSOR_OBSERVER_H

class CursorSubject;

#include "Location.h"

class CursorObserver
{
public:
    virtual void update(Location const& state) = 0;

    virtual ~CursorObserver() = default;
};

bool operator==(CursorObserver const& lhs, CursorObserver const& rhs);
bool operator!=(CursorObserver const& lhs, CursorObserver const& rhs);

#endif // !CURSOR_OBSERVER_H
