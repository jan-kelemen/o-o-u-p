#include "lab0302.h"

LAB0302::LAB0302(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    editor = new TextEditor(this);
    setCentralWidget(editor);
}

void LAB0302::keyPressEvent(QKeyEvent* e)
{
    editor->keyPressEvent(e);
}
