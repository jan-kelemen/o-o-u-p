#ifndef EDIT_ACTION_OBSERVER_H

#define EDIT_ACTION_OBSERVER_H

class EditActionSubject;

class EditActionObserver
{
public:
    virtual void update(EditActionSubject& subject) = 0;

    virtual ~EditActionObserver() = default;
};

bool operator==(EditActionObserver const& lhs, EditActionObserver const& rhs);
bool operator!=(EditActionObserver const& lhs, EditActionObserver const& rhs);

#endif // !EDIT_ACTION_OBSERVER_H
