#ifndef DELETE_AFTER_ACTION_H

#define DELETE_AFTER_ACTION_H

#include <memory>

#include "EditAction.h"
#include "Location.h"

class DeleteAfterAction : public EditAction
{
public:
    static bool executable(TextEditorModel& model);

    DeleteAfterAction(TextEditorModel& model);

    virtual void execute() override;

    virtual void revert() override;
private:
    std::unique_ptr<EditAction> delete_range_;

    char c_;
    Location location_;
};

#endif // !DELETE_AFTER_ACTION_H
